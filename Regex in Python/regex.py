import sys

""" Takes input arguments: a regex pattern and a string respectively 
	and prints whether regex matched or not. 
	Run as: regex.py <pattern> <string>
"""

def match_exp(exp, pattern):
	count = 0 # tracks the current letter in exp
	for letter in pattern:
		# if the expression ends before the pattern return false
		if(len(exp) <= count):
			return False
		elif(letter == '.'):
			count += 1
		elif(letter == '*'):
			# If the pattern starts with *, it is an incorrect pattern and will return false
			if(count == 0):
				print("Error: Incorrect Pattern")
				exit(1)

			preceding_letter = exp[count-1]
			while(len(exp) > count and exp[count] == preceding_letter):
				count += 1
		elif(exp[count] != letter):
			return False
		else:
			count += 1
	#If the pattern ends before the expression return false		
	if(len(exp) > count):
			return False
	return True




if __name__ == '__main__':
	if(len(sys.argv)!= 3):
		print("Error: Need to provide arguments for pattern and expression respectively")
		exit(1)
	exp = sys.argv[2]
	pattern = sys.argv[1]
	return_val = match_exp(exp, pattern)
	if(return_val):
		print("Regex matches")
	else:
		print("Regex does not match")


