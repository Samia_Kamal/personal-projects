#include <stdio.h> 

struct node{
	struct node* next; 
	int val; 
};

typedef struct list{
	struct node* head;
}list;

void initialize_list(list* mylist){
	mylist->head = NULL;
	
}

void add_list(list* mylist, int data){
	if(mylist->head == NULL){
		mylist->head = (struct node*)malloc(sizeof(struct node));
		mylist->head->val = data;
		mylist->head->next == NULL;
	}
	else{
		struct node* traverseptr = mylist->head; 
		while(traverseptr->next!= NULL){
			traverseptr  = traverseptr->next;
		}
			traverseptr-> next = (struct node*)malloc(sizeof(struct node));
			traverseptr = traverseptr->next; 
			traverseptr->val = data; 
			traverseptr->next = NULL;
	}
}
int remove_list(list* mylist){
	struct node* head = mylist->head; 
	if(head == NULL){
			printf("ERROR: Trying to remove from an empty list\n");
			exit(0);
	}	
	else{
			struct node* temp = head;
			mylist->head = head->next; 
			int retval = temp->val;
			free(temp);
			return retval;
	}	
}

int main(){
	list l1;
	initialize_list(&l1);
	add_list(&l1, 5);
	add_list(&l1, 6);
	int value = remove_list(&l1);
	printf("%d\n", value);	
}
