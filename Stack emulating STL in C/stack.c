#include <stdio.h>

#define MAXELEM 1 //Max numer of elements held by stack


/* Create a generic stack called basestack which stores any data type. 
   Basestack will then be cast to a struct "stack" which stores the right data type
   User will use the interface for struct "stack"*/

typedef struct{
	void* stackarr; 
	int numelem;
	int maxbytes;

} Basestack; 


#define Stack(type)\
	struct{\
		type* stackarr;\
		int numelem;\
		int maxbytes;\
		}*

/* constructor for Stack - Create a Basestack object and cast it to Stack */
#define new_stack(type, bytes) (Stack(type)) create_basestack(bytes)

Basestack* create_basestack(int bytes){
	Basestack *mybasestack = malloc(sizeof(Basestack*));
	mybasestack->stackarr = malloc(MAXELEM*bytes);
	mybasestack->numelem = 0;
	mybasestack->maxbytes = MAXELEM*bytes;
}	
	
/*
Push an item onto the stack. If the stack is full - already contains max number of elements, resize it
Do while loop used to expand macro as 1 statement for safer macro use
*/
#define push(stack, data)\
	do{\
		if(stack->numelem >= MAXELEM)\
			resize(stack, stack->maxbytes *2);\
		stack->stackarr[stack->numelem] = data;\
		stack->numelem ++;\
	}while(0)

/* Pop an item from the stack. If the stack is empty, free it an exit with an error. 
   Parameters: a pointer to the variable that you want the popped result to be stored into
	       the stack you are popping from 

Do while loop used to expand macro as 1 statement for safer macro use
*/
#define pop(popval, stack)\
	do{\
		if(stack->numelem <= 0){\
			printf("ERROR: Popping an empty stack\n");\
			delete_stack(stack);\
			exit(0);\
		}\
		else{\
			stack->numelem --;\
			popval = &(stack->stackarr[stack->numelem]);\
		}\
	}while(0)
	
/*Reallocate more space for the stack*/
void resize(Basestack* mybasestack, int size){
	mybasestack->stackarr = realloc(mybasestack->stackarr, size);
}

/*Free the stack to prevent memory leaks*/
void delete_stack(Basestack* mybasestack){
	free(mybasestack->stackarr);
	free(mybasestack);
}
 
int main(){

	//Create a stack of integers 
	Stack(int) stack1 = new_stack(int, sizeof(int));
	//push 20
	push(stack1, 20);
	//Push 30
	push(stack1, 30);
	int *val;
	pop(val, stack1);
	printf("Popped %d\n", *val); 	
	pop(val, stack1);
	printf("Popped %d\n", *val);
	//pop(val, stack1); //Verify that popping an empty stack returns error
	delete_stack(stack1);

}
