import requests
import xml.etree.ElementTree as ET

latitude = -122.880038 # Latitude of my house 
longitude = 49.141153 # Longitude of my house

def getStopEstimates(route, street, destination):
    # Get the stops for the 319 within a 2 km radius
    url = 'https://api.translink.ca/rttiapi/v1/stops?apikey=GMkoc4pROHAxPwgOATZ1&lat={}&long={}&routeNo={}&radius=2000'.format(longitude,latitude, route)
    #print(url)
    r = requests.get(url)

    root = ET.fromstring(r.content)
    #print(r.content)
    #print(root.tag)
    stopNumber = []
    for stop in root.iter('Stop'):
        for stopNo in stop.iter('StopNo'):
            #print(stopNo.tag, stopNo.text)
            continue
        for atStreet in stop.iter('AtStreet'):
            #print(atStreet.tag, atStreet.text)
            continue
        for onStreet in stop.iter('OnStreet'):
            #print(onStreet.tag, onStreet.text)
            continue
        # Filter based on the specified street
        if onStreet.text.lower() == street.lower() or atStreet.text.lower() == street.lower():
                #print("THIS IS THE RIGHT STOP !!")
                stopNumber.append(stopNo.text)
    print(tuple(stopNumber))
    busTimings = []
    for stopNo in stopNumber:
        print(stopNo)
        url = 'https://api.translink.ca/rttiapi/v1/stops/{}/estimates?apikey=GMkoc4pROHAxPwgOATZ1&count=3&timeframe=120'.format(stopNo)
        r = requests.get(url)
        #print(r.content)
        root = ET.fromstring(r.content)
        for schedule in root.iter('Schedule'):
            # Filter stops based on the destination and return the expected leave time for the correct stop
            for dest in schedule.iter('Destination'):
                if dest.text.lower() == destination.lower():
                    for leaveTime in schedule.iter('ExpectedLeaveTime'):
                        print('\n' + dest.text)
                        busTimings.append(leaveTime.text)
                        print(leaveTime.text)
    return tuple(busTimings)

if __name__ == '__main__':
    route = 319
    street = '130 st'
    destination = 'SCOTT RD STN'
    #stopNumber = getStopNumber(route, street)
    #print(stopNumber)
    #busTimes = getStopEstimate(stopNumber, destination)
    busTimes = getStopEstimates(route, street, destination)
    print(busTimes)