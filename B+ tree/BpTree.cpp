#include "BpTree.h"

BpTree::BpTree(int num)
{
	////Create a leaf node as the root
	root = new LeafNode(num);
}

/*
Destructor - Traverses tree bottom up and deletes all the node objects
*/
BpTree::~BpTree()
{
	Node* TraversePtr = root;
	while (!(TraversePtr->getNodeType() == "LeafNode")) {
		TraversePtr = (TraversePtr->getChildNodePtr())[0];
	}
	Node* parent = *(TraversePtr->getParent());
	Node* temp = TraversePtr;
	while (parent != NULL){
		do {
			TraversePtr = *(TraversePtr->getRightSiblingPtr());
			delete temp;
			temp = TraversePtr;
		} while (TraversePtr != NULL);
		TraversePtr = parent; 
		parent = *(TraversePtr->getParent());
		temp = TraversePtr;

	}
	delete TraversePtr;

}

/*
Inserts the key and value into the tree unless if the key is already in the tree
Input: int key, string value
Output: true if insertion successful, false otherwise
*/
bool BpTree::insert(int key, string value)
{
	Node* traverseptr = this->searchLeafNode(key);
	if (traverseptr->getNodeType() == "LeafNode") { //can probably remove this if statement
		int* numElementsPtr = traverseptr->getNumElementsPtr();
		int* keysptr = traverseptr->getKeysPtr();
		string* recordsptr = traverseptr->getRecords();
		int numkeys = traverseptr->getNumKeys();
		if (*numElementsPtr < numkeys) {
			//Search leaf node for correct place to insert key (in order) and insert key in place
			int i = 0;
			for (i; i < *numElementsPtr; i++) {
				if (keysptr[i] < key) {
					continue;
				}
				else if (keysptr[i] == key) {
					return false;
				}
				else {
					for (int j = *numElementsPtr; j > i; j--) {
						keysptr[j] = keysptr[j - 1];
						recordsptr[j] = recordsptr[j - 1];
					}
					break;
				}
			}
			keysptr[i] = key;
			recordsptr[i] = value;
			*numElementsPtr += 1;
			return true;
		}
		else {
			// The leaf node is full and the number of elements in the node == number of keys allowed
			//determine if the key is already in the node. If it is return false
			for (int i =0; i < *numElementsPtr; i++) {
				if (keysptr[i] == key) { return false; }
			}
			// Create a new node for redistributing values
			int maxElems = numkeys - 1; //tracks which value in left node we are moving 
			bool inserted = false; // tracks if key has been inserted in new right node
			Node* newRightNode = new LeafNode(numkeys);
			int* rightNodeKeysptr = newRightNode->getKeysPtr();
			string* rightNodeRecordsptr = newRightNode->getRecords();
			Node** parentptr = traverseptr->getParent();
			//assign parent & sibling pointers of new right node
			*(newRightNode->getRightSiblingPtr()) = *(traverseptr->getRightSiblingPtr());
			*(traverseptr->getRightSiblingPtr()) = newRightNode; //right node is sibling of left node
			*(newRightNode->getParent()) = *parentptr;
			*(newRightNode->getLeftSiblingPtr()) = traverseptr; // assign left sibling of new node

			//set num elements contained in each node
*numElementsPtr = ceil((float(numkeys) + 1) / 2);
*(newRightNode->getNumElementsPtr()) = (numkeys + 1) / 2;
int leftIndex = ceil((float(numkeys) + 1) / 2) - 1; //last index of left node
int rightIndex = ((numkeys + 1) / 2) - 1; //last index of right node

//move values from left node to right node or insert key into right node
for (int i = rightIndex; i >= 0; i--) {
	if (keysptr[maxElems] > key || inserted == true) {
		//move values to right node
		rightNodeKeysptr[i] = keysptr[maxElems];
		rightNodeRecordsptr[i] = recordsptr[maxElems];
		maxElems--;
	}
	else {
		//insert key into left node
		rightNodeKeysptr[i] = key;
		rightNodeRecordsptr[i] = value;
		inserted = true;
	}
}
// if key was not inserted into right node, need to shift values in left node and insert key in right spot
// same algorithm as inserting into non-full leaf node
if (inserted == false) {
	int i = 0;
	for (i; i <= leftIndex; i++) {
		if (keysptr[i] < key) {
			continue;
		}
		else {
			for (int j = leftIndex; j > i; j--) {
				keysptr[j] = keysptr[j - 1];
				recordsptr[j] = recordsptr[j - 1];
			}
			break;
		}
	}
	keysptr[i] = key;
	recordsptr[i] = value;
}

//If leaf node does not have parent, create a new one
if (*parentptr == NULL) {
	createParent(traverseptr, newRightNode, rightNodeKeysptr[0]);
}
else {
	insertInteriorNode(*parentptr, rightNodeKeysptr[0], newRightNode);
}

		}
	}
	return true;
}

/*
Removes the key from the tree and associated value from the tree. 
Input: Key to remove (int)
Output: True if removal successful. False otherwise
*/
bool BpTree::remove(int key)
{
	Node* leaf = searchLeafNode(key);
	int* numElemPtr = leaf->getNumElementsPtr();
	int* keysPtr = leaf->getKeysPtr();
	string* recordsPtr = leaf->getRecords();
	int numkeys = leaf->getNumKeys();
	bool removed = false;
	//Remove the key from the leaf node
	for (int i = 0; i < *numElemPtr; i++) {
		if (key == keysPtr[i]) {
			for (int j = i; j < (*numElemPtr - 1); j++) {
				keysPtr[j] = keysPtr[j + 1];
				recordsPtr[j] = recordsPtr[j + 1];
			}
			*numElemPtr = *numElemPtr - 1;
			removed = true;
			break; //return true?? 
		}
	}
	//Did not find a key to remove. Thus return false
	if (removed == false) { return false; }

	//The node is now less than half empty -- need to redistribute or coalesce
	if (*numElemPtr < ceil(float(numkeys) / 2)) {
		Node* leftSib = *(leaf->getLeftSiblingPtr());
		Node* rightSib = *(leaf->getRightSiblingPtr());

		//Check if you can redistribute from left node 
		if (leftSib != NULL && ((*(leftSib->getNumElementsPtr()) + *numElemPtr) / 2 >= ceil(float(numkeys) / 2))){
			int* leftSibKeysPtr = leftSib->getKeysPtr();
			string* leftSibRecordsPtr = leftSib->getRecords();
			//Redistribute from left node
			int insertAmount = ceil(float(numkeys) / 2) - *numElemPtr; //number of keys we are moving into right node
			int* leftSibNumElem = leftSib->getNumElementsPtr();
			 
			//shift values in right node to make room for new values 
			for (int j = (*numElemPtr - 1); j >= 0; j--) {
				keysPtr[j + insertAmount] = keysPtr[j];
				recordsPtr[j + insertAmount] = recordsPtr[j];
			}
			//Insert values in right node from left node
			for (int j = 0; j < insertAmount; j++) {
				int indexToInsert = *leftSibNumElem - (j + 1);
				keysPtr[(insertAmount - (j + 1))] = leftSibKeysPtr[indexToInsert];
				recordsPtr[(insertAmount - (j + 1))] = leftSibRecordsPtr[indexToInsert];
			}
			//Assign new num elem
			*leftSibNumElem = *leftSibNumElem - insertAmount;
			*numElemPtr = *numElemPtr + insertAmount;
			//Change key value in parent Node
			if (*(leaf->getParent()) != NULL) {
				Node* parentNode = *(leaf->getParent()); 
				int* parentKeysPrt = parentNode->getKeysPtr();
				for (int i = *(parentNode->getNumElementsPtr()) - 1; i >= 0; i--) {
					if (parentKeysPrt[i] <= keysPtr[0]) {
						parentKeysPrt[i] = keysPtr[0];
					}
				}
			}

		}
		//check if can redistribute from right node
		else if (rightSib != NULL && ((*(rightSib->getNumElementsPtr()) + *numElemPtr) / 2 >= ceil(float(numkeys) / 2))) {
			int* rightSibKeysPtr = rightSib->getKeysPtr();
			string* rightSibRecordsPtr = rightSib->getRecords();
			//Redistribute from right node
			int insertAmount = ceil(float(numkeys) / 2) - *numElemPtr; //number of keys we are moving into left node
			int* rightSibNumElem = rightSib->getNumElementsPtr();

			//Insert values in left node from right node
			for (int j = 0; j < insertAmount; j++) {
				keysPtr[*numElemPtr + j] = rightSibKeysPtr[j];
				recordsPtr[*numElemPtr + j] = rightSibRecordsPtr[j];
			}
			//shift values in right node 
			for (int j = 0; j <(*rightSibNumElem - insertAmount); j++) {
				rightSibKeysPtr[j] = rightSibKeysPtr[j + insertAmount];
				rightSibRecordsPtr[j] = rightSibRecordsPtr[j + insertAmount];
			}
			
			//Assign new num elem
			*rightSibNumElem = *rightSibNumElem - insertAmount;
			*numElemPtr = *numElemPtr + insertAmount;
			//Change key value in parent Node
			if (*(leaf->getParent()) != NULL) {
				Node* parentNode = *(leaf->getParent());
				int* parentKeysPrt = parentNode->getKeysPtr();
				for (int i = *(parentNode->getNumElementsPtr()) - 1; i >= 0; i--) {
					if (parentKeysPrt[i] <= rightSibKeysPtr[0]) {
						parentKeysPrt[i] = rightSibKeysPtr[0];
					}
				}
			}
		}
		//coalescing left sibling
		else if (leftSib != NULL) {
			int* leftSibKeysPtr = leftSib->getKeysPtr();
			string* leftSibRecordsPtr = leftSib->getRecords();
			int* leftSibNumElem = leftSib->getNumElementsPtr();
			//move values from right node to left node
			for (int i = 0; i < *numElemPtr; i++) {
				leftSibKeysPtr[*leftSibNumElem + i] = keysPtr[i];
				leftSibRecordsPtr[*leftSibNumElem + i] = recordsPtr[i];
			}
			*leftSibNumElem = *leftSibNumElem + *numElemPtr;
			removeInteriorNode(*(leaf->getParent()), leaf);
		}
		//coalescing right sibling
		else if (rightSib != NULL) {
			int* rightSibKeysPtr = rightSib->getKeysPtr();
			string* rightSibRecordsPtr = rightSib->getRecords();
			int* rightSibNumElem = rightSib->getNumElementsPtr();
			//shifting values right
			for (int i = 0; i < *rightSibNumElem; i++) {
				rightSibKeysPtr[*rightSibNumElem + *numElemPtr -1-i] = rightSibKeysPtr[*rightSibNumElem-1 - i];
				rightSibRecordsPtr[*rightSibNumElem+ *numElemPtr-1-i] = rightSibRecordsPtr[*rightSibNumElem - i];
			}
			//move values from left node to right node
			for (int i = 0; i < *numElemPtr; i++) {
				rightSibKeysPtr[i] = keysPtr[i];
				rightSibRecordsPtr[i] = recordsPtr[i];
			}
			*rightSibNumElem = *rightSibNumElem + *numElemPtr;
			removeInteriorNode(*(leaf->getParent()), leaf);
		}
	}
	return true; 
}

/*
Searches tree for the valuse associated with the key 
Input: key (int)
Output: Value stored at key. False if the key is not in tree.
*/
string BpTree::find(int key)
{
	Node* verticalTraversePtr = root;
	Node* horizontalTraversePtr = root;
	//print the keys for the interior level nodes
	while (!(verticalTraversePtr->getNodeType() == "LeafNode")) {
		verticalTraversePtr = (verticalTraversePtr->getChildNodePtr())[0];
		}
	horizontalTraversePtr = verticalTraversePtr;
	//Print the keys in the leaf level nodes 
	do {
		int numElem = *(horizontalTraversePtr->getNumElementsPtr());
		int* keysptr = horizontalTraversePtr->getKeysPtr();
		string* recordsptr = horizontalTraversePtr->getRecords();
		for (int i = 0; i < numElem; i++) {
			if (keysptr[i] == key) {
				return recordsptr[i];
			}
		}
		horizontalTraversePtr = *(horizontalTraversePtr->getRightSiblingPtr());
	} while (horizontalTraversePtr != NULL);
	return "";
}

/*
Print the keys of each node. All keys on one level are written on the same line
*/
void BpTree::printKeys()
{
	Node* verticalTraversePtr = root;
	Node* horizontalTraversePtr = root; 
	//print the keys for the interior level nodes
	while (!(verticalTraversePtr ->getNodeType() == "LeafNode")) {
		do {
			int numElem = *(horizontalTraversePtr->getNumElementsPtr());
			int* keysptr = horizontalTraversePtr->getKeysPtr();
			cout << "["; 
			for (int i = 0; i < numElem; i++) {
				cout << keysptr[i]; 
				if ((numElem - i) > 1) {
					cout << ",";
				}
			}
			cout << "] ";
			horizontalTraversePtr = *(horizontalTraversePtr->getRightSiblingPtr());
		} while (horizontalTraversePtr != NULL);
		cout << endl;
		verticalTraversePtr = (verticalTraversePtr->getChildNodePtr())[0];
		horizontalTraversePtr = verticalTraversePtr;
	}
	//Print the keys in the leaf level nodes 
	do {
		int numElem = *(horizontalTraversePtr->getNumElementsPtr());
		int* keysptr = horizontalTraversePtr->getKeysPtr();
		cout << "[";
		for (int i = 0; i < numElem; i++) {
			cout << keysptr[i];
			if ((numElem - i) > 1) {
				cout << ",";
			}
		}
		cout << "] ";
		horizontalTraversePtr = *(horizontalTraversePtr->getRightSiblingPtr());
	} while (horizontalTraversePtr != NULL);
}

/*
Traverses leaf level and prints record values from left to right
*/
void BpTree::printValues()
{
	Node* traverseptr = root; 
	while (!(traverseptr->getNodeType() == "LeafNode")) {
		traverseptr = (traverseptr->getChildNodePtr())[0];
	}
	// go through each leaf node and print it's values, traversing through right sibling pointer
	do{
		int numElem = *(traverseptr->getNumElementsPtr());
		string* recordsptr = traverseptr->getRecords();
		for (int i = 0; i < numElem; i++) {
			cout << recordsptr[i] << endl;
		}
		traverseptr = *(traverseptr->getRightSiblingPtr());
	} while (traverseptr != NULL);
}

/*
Returns a pointer pointing to a leaf node containing the input key
input: key (int)
Output: Pointer to the leaf node containing the key
*/
Node* BpTree::searchLeafNode(int key) {
	Node* traverseptr = root; 
	while(!(traverseptr->getNodeType() == "LeafNode")) {
		int numElem = *(traverseptr->getNumElementsPtr());
		int* keys = traverseptr->getKeysPtr();
		Node** childptr = traverseptr->getChildNodePtr();
		for (int i = numElem-1; i >= 0; i--) {
			//Key is in the first child
			if (i == 0 && keys[i] > key) {
				traverseptr = childptr[i];
				break;
			}
			//key is in the pointer right of the keys[i] value
			else if ((i == 0 && keys[i] <= key) || keys[i] <= key) {
				traverseptr = childptr[i + 1];
				break;
			}
			//key is smaller than the current keys[i] value
			else { continue; }
		}
	}
	return traverseptr;
}

/*
Inserts serach key value into an interior level node. Propogates the insertion up the tree if necessary. 
Input: Node in which we insert key,  key to insert into the node, child pointer for the associated key 
*/
void BpTree::insertInteriorNode(Node* insertionNode, int key, Node* rightNode) {
	int* numElemPtr = insertionNode->getNumElementsPtr();
	int numKeys = insertionNode->getNumKeys();
	int* keysptr = insertionNode->getKeysPtr();
	Node** childptr = insertionNode->getChildNodePtr();
	Node** rightNodeParent = rightNode->getParent();
	*rightNodeParent = insertionNode; //rightnode has the insertion node as the parent
	//insert key into interior node if the node is not full
	if (*numElemPtr < numKeys) {
		int i = 0;
		for (i; i < *numElemPtr; i++) {
			if (keysptr[i] < key) {
				continue;
			}
			else {
				for (int j = *numElemPtr; j > i; j--) {
					keysptr[j] = keysptr[j - 1];
					childptr[j+1] = childptr[j];
				}
				break;
			}
		}
		keysptr[i] = key;
		childptr[i + 1] = rightNode;
		int numelem = *numElemPtr += 1;
	}
	else {
		//The node is full and needs to be split
		// Create a new node for redistributing values
		int maxElems = numKeys - 1; //tracks which value in left node we are moving 
		bool inserted = false; // tracks if key has been inserted in new right node
		Node* newRightNode = new InteriorNode(numKeys);
		int* rightNodeKeysptr = newRightNode->getKeysPtr();
		Node** rightNodeChildptr = newRightNode->getChildNodePtr();
		Node** parentptr = insertionNode->getParent();
		//Assign parent and sibling pointer to new right node
		*(newRightNode->getRightSiblingPtr()) = *(insertionNode->getRightSiblingPtr());
		*(insertionNode->getRightSiblingPtr()) = newRightNode; //right node is sibling of left node
		*(newRightNode->getParent()) = *parentptr;
		*(newRightNode->getLeftSiblingPtr()) = insertionNode;

		//set num elements contained in each node
		*numElemPtr = ceil(float(numKeys) / 2);
		*(newRightNode->getNumElementsPtr()) = numKeys/2;

		int leftIndex = ceil(float(numKeys)/2)-1; //last index of left node
		int rightIndex = (numKeys/2)-1; //last index of right node

	 //move values from left node to right node or insert key into right node
		for (int i = rightIndex; i >=0; i--) {
			if (keysptr[maxElems] > key || inserted == true) {
				//move values to right node
				rightNodeKeysptr[i] = keysptr[maxElems];
				rightNodeChildptr[i+1] = childptr[maxElems+1];
				*(rightNodeChildptr[i + 1]->getParent()) = newRightNode;
				maxElems--;
				if (i == 0) {
					rightNodeChildptr[i] = childptr[maxElems];
					*(rightNodeChildptr[i]->getParent()) = newRightNode;
				}
			}
			else {
				//insert key into left node
				rightNodeKeysptr[i] = key;
				rightNodeChildptr[i+1] = rightNode;
				inserted = true;
			}
		}

		// if key was not inserted into right node, need to shift values in left node and insert key in right spot
		if (inserted == false) {
			int i = 0;
			for (i; i < *numElemPtr; i++) {
				if (keysptr[i] < key) {
					continue;
				}
				else {
					for (int j = *numElemPtr; j > i; j--) {
						keysptr[j] = keysptr[j - 1];
						childptr[j + 1] = childptr[j];
					}
					break;
				}
			}
			keysptr[i] = key;
			childptr[i + 1] = rightNode;
		}

		//Insert key into parent node
		if (*(insertionNode->getParent()) == NULL) {
			createParent(insertionNode, newRightNode, keysptr[*numElemPtr]);
		}
		else {
			insertInteriorNode(*(insertionNode->getParent()), keysptr[*numElemPtr], newRightNode);
		}
			
	}
}

/*
Creates a parent node. 
Input: Left child node, right child node, key
*/
void BpTree::createParent(Node* leftNode, Node* rightNode, int key) {
	int numkeys = leftNode->getNumKeys();
	int* rightNodeKeysptr = rightNode->getKeysPtr();
	Node* parentNode = new InteriorNode(numkeys);
	int* parentKeysptr = parentNode->getKeysPtr();
	int* parentNumElemPtr = parentNode->getNumElementsPtr();
	Node** childArrayPtr = parentNode->getChildNodePtr();
	parentKeysptr[0] = key;
	childArrayPtr[0] = leftNode;
	childArrayPtr[1] = rightNode;
	*parentNumElemPtr = *parentNumElemPtr + 1;
	*(leftNode->getParent()) = parentNode;
	*(rightNode->getParent()) = parentNode;
	root = parentNode;
}

/*
Removes search key value and child node from an interior level node.
Input: Node in which we remove child, child node to delete
*/
void BpTree::removeInteriorNode(Node* removalNode, Node* childToRemove) {
	int* keysPtr = removalNode->getKeysPtr();
	Node** childPtr = removalNode->getChildNodePtr();
	int* numElem = removalNode->getNumElementsPtr();
	for (int i = 0; i <= *numElem; i++) {
		if (childPtr[i] == childToRemove) {
			//shift all child pointer values left
			for (int j = i; j < *numElem; j++) {
				childPtr[j] = childPtr[j+1];
			}
			//shift all keysptr values left
			for (int j = i-1; j < *numElem-1; j++) {
				if (j == -1) {
					continue;
				}
				keysPtr[j] = keysPtr[j + 1];
			}
			*numElem = *numElem - 1; 
			Node* leftsibling = *(childToRemove->getLeftSiblingPtr());
			Node* rightSibling = *(childToRemove->getRightSiblingPtr());
			if (leftsibling != NULL) {
				*(leftsibling->getRightSiblingPtr()) = rightSibling;
			}
			if (rightSibling != NULL) {
				*(rightSibling->getLeftSiblingPtr()) = leftsibling;
			}
			delete childToRemove;
			
		}
		
	}
}