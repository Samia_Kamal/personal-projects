#ifndef NODE_H
#define NODE_H
#pragma once
#include <string>
#include <iostream>
using namespace std;


class Node
{
public:
	Node();
	~Node();
	virtual Node** getParent();//returns a pointer to the parent of a node
	virtual string getNodeType();//returns node type
	virtual string* getRecords(); //returns array of strings
	virtual Node** getRightNode(); //returns pointer to a pointer to a node
	virtual int* getNumElementsPtr(); //returns pointer to the # of elements currently in array
	virtual Node** getChildNodePtr(); //returns pointer to an array of child nodes
	virtual int* getKeysPtr(); //returns pointer to array of keys
	virtual int getNumKeys(); //returns # of slots in a node
	virtual Node** getRightSiblingPtr(); // returns a pointer the right sibling of node
	virtual int* getNumChildrenPtr(); // returns a pointer to the child siblings
	virtual Node** getLeftSiblingPtr(); // returns a pointer the left sibling of node
public:
	string nodeType;
};

class LeafNode: public Node
{
public:
	LeafNode(int);
	~LeafNode();	

	Node** getParent();
	string getNodeType();
	string* getRecords();
	Node** getRightNode();
	int* getKeysPtr();
	int* getNumElementsPtr();
	int getNumKeys(); 
	Node** getRightSiblingPtr();
	Node** getLeftSiblingPtr();
public:
	Node* parent;
	string nodeType;
	string* records;
	Node* rightNode;
	int* keys;
	int numElements;
	int numKeys;
	Node* rightSibling;
	Node* leftSibling;

};
class InteriorNode: public Node
{
public:
	InteriorNode(int);
	~InteriorNode();

	Node** getParent();
	string getNodeType();
	Node** getChildNodePtr();
	int* getKeysPtr();
	int* getNumElementsPtr();
	int getNumKeys();
	Node** getRightSiblingPtr();
	Node** getLeftSiblingPtr();
	int* getNumChildrenPtr();
public:
	Node* parent;
	string nodeType;
	Node** childNodes;
	int* keys;
	int numKeys;
	int numElements;
	Node* rightSibling;
	Node* leftSibling;
	int numChildren;
};
#endif /*NODE_H*/
