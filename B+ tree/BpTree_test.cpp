#include "BpTree.h"
using namespace std;

int main()
{
	cout << "In main" << endl;
	BpTree tree = BpTree(5);
	tree.insert(1, "record 1");
	tree.printKeys();
	tree.insert(4, "record 4");
	tree.printKeys();
	tree.insert(9, "record 9");
	tree.printKeys();
	tree.insert(7, "record 7");
	tree.printKeys();
	tree.insert(3, "record 3");
	tree.printKeys();
	tree.insert(5, "record 5");
	tree.printKeys();
	tree.insert(10, "record 10");
	tree.printKeys();
	tree.insert(2, "record 2");
	tree.printKeys();
	tree.insert(8, "record 8");
	tree.printKeys();
	tree.insert(0, "record 0");
	tree.printKeys();
	tree.insert(6, "record 6");
	tree.printKeys();
	tree.insert(11, "record 11");
	tree.printKeys();
	tree.insert(12, "record 12");
	tree.printKeys();
	tree.insert(13, "record 13");
	tree.printKeys();
	tree.insert(14, "record 14");
	tree.printKeys();
	tree.insert(15, "record 15");
	tree.printKeys();
	tree.insert(16, "record 16");
	tree.printKeys();
	tree.insert(17, "record 17");
	tree.printKeys();
	tree.insert(18, "record 18");
	tree.printKeys();
	tree.insert(19, "record 19");
	tree.printKeys();
	tree.insert(20, "record 20");
	tree.printKeys();
	tree.insert(21, "record 21");
	tree.printKeys();
	tree.insert(22, "record 22");
	tree.insert(23, "record 23");
	tree.insert(24, "record 24");
	tree.insert(25, "record 25");
	//tree.insert(26, "record 26");
	//tree.insert(27, "record 27");
	tree.printKeys();
	//Node* traverseptr = tree.searchLeafNode(7);
	//cout << traverseptr << endl; 
	tree.printValues();
	tree.printKeys();
	cout << tree.find(9) << endl;
	cout << tree.find(1) << endl;
	cout << tree.find(5) << endl;
	cout << tree.find(17) << endl;
	cout << tree.find(100) << endl;
	cout << tree.find(-1) << endl;
	cout << tree.find(24) << endl;
	//cout << "remove 4: " << tree.remove(4) << endl << endl;
	//tree.printKeys();
	cout << "remove 2: " << tree.remove(2) << endl << endl;
	tree.printKeys();
	cout << "remove 3: " << tree.remove(3) << endl << endl;
	tree.printKeys();
	cout << "remove 4: " << tree.remove(4) << endl << endl;
	/*tree.printKeys();
	cout <<"remove 0: " <<  tree.remove(0) << endl;
	tree.printKeys();
	cout <<"remove 4" <<  tree.remove(4) << endl;*/
	tree.printKeys();


	return 0;

}