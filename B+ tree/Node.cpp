#include "Node.h"

Node::Node() 
{
	nodeType = "Node";
}
Node** Node::getParent()
{
	return NULL;
}
string Node::getNodeType() 
{
	return nodeType;
}
string* Node::getRecords()
{
	return NULL;
}
Node** Node::getRightNode()
{
	return NULL; 
}
int* Node::getNumElementsPtr()
{
	return NULL;
}
Node** Node::getChildNodePtr()
{
	return NULL;
}
int* Node::getKeysPtr()
{
	return NULL;
}
int Node::getNumKeys()
{
	return -1;
}
Node** Node::getRightSiblingPtr() {
	return NULL;
}
Node** Node::getLeftSiblingPtr() {
	return NULL;
}
int* Node::getNumChildrenPtr() {
	return NULL;
}
Node::~Node()
{

}

InteriorNode::InteriorNode(int num)
{
	nodeType = "InteriorNode";
	numKeys = num;
	keys = new int[num];
	childNodes = new Node*[num + 1];
	parent = NULL; 
	numElements = 0;
	rightSibling = NULL;
	leftSibling = NULL;
	numChildren = 0; 
}

string InteriorNode::getNodeType()
{
	return nodeType;
}


Node** InteriorNode::getParent()
{
	return &parent;
}

Node** InteriorNode::getChildNodePtr()
{
	return childNodes; 
}

int* InteriorNode::getKeysPtr()
{
	return keys;
}
int* InteriorNode::getNumElementsPtr()
{
	return &numElements;
}
//returns the number of slots in a node
int InteriorNode::getNumKeys()
{
	return numKeys;
}
Node** InteriorNode::getRightSiblingPtr() {
	return &rightSibling;
}
Node** InteriorNode::getLeftSiblingPtr() {
	return &leftSibling;
}
int* InteriorNode::getNumChildrenPtr() {
	return &numChildren;
}
InteriorNode::~InteriorNode()
{
	delete[] keys;
	delete[] childNodes;
}


LeafNode::LeafNode(int num)
{
	nodeType = "LeafNode"; 
	numKeys = num; 
	keys = new int[num]; 
	rightNode = NULL;
	parent = NULL;
	records = new string[num];
	numElements = 0;
	rightSibling = NULL;
	leftSibling = NULL;
}

Node** LeafNode::getParent() 
{
	return &parent;
}

string LeafNode::getNodeType()
{
	return nodeType;
}
string* LeafNode::getRecords()
{
	return records;
}
Node** LeafNode::getRightNode()
{
	return &rightNode;
}
int* LeafNode::getKeysPtr()
{
	return keys;
}
int* LeafNode::getNumElementsPtr()
{
	return &numElements;
}
int LeafNode::getNumKeys()
{
	return numKeys;
}
Node** LeafNode::getRightSiblingPtr() {
	return &rightSibling;
}
Node** LeafNode::getLeftSiblingPtr() {
	return &leftSibling;
}
LeafNode::~LeafNode()
{
	delete[] keys; 
	delete[] records;
}
