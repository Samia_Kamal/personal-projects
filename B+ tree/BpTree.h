#ifndef BPTREE_H
#define BPTREE_H
#pragma once
#include <string>
#include <iostream>
#include "Node.h"
using namespace std;

class BpTree
{
public:
	BpTree(int num);
	~BpTree();
	bool insert(int, string);
	bool remove(int); 
	string find(int); 
	void printKeys(); 
	void printValues();
	Node* searchLeafNode(int);
	void insertInteriorNode(Node*, int, Node*);
	void createParent(Node*, Node*, int);
	void removeInteriorNode(Node*, Node*);
public: 
	Node* root;
};
#endif /*BPTREE_H*/
