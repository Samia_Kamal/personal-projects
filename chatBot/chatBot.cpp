#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <queue>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <bits/stdc++.h>
#include <errno.h>
#include <thread>
#define BACKLOG 10 // number of pending connections listening queue can hold

using namespace std;

/*
	Takes a queue containing messages and writes it to the screen.

	* Using cin and cout because reasearch tells me they might be threadsafe. printf and scanf are not threadsafe, so would want to use write() and read() funtions if written in C. But since written in C++. I do not know if cin and cout are threadsafe. 
*/

#define MAXDATASIZE 100 
#define NUMTHREADS 1

void PrintMessage(queue <char*> &incoming_queue){
	while(1){
        	if(!incoming_queue.empty()){
        	        cout << '~' << incoming_queue.front() << endl;
        	        incoming_queue.pop();
        	}
	}
	
}

void ReadKeyboard(queue <char*> &outgoing_queue){
	char input[MAXDATASIZE]; 
	cin.getline(input, sizeof(input));
	while(strcmp(input, "exit()") != 0){
		outgoing_queue.push(input);
		
		cin.getline(input, sizeof(input));
	}
}

void *get_in_addr(struct sockaddr *sa){
        // gets the socket address: returns an ipv4 or ipv6 value
        if(sa->sa_family == AF_INET){
                return &(((struct sockaddr_in*)sa)-> sin_addr); // return the address of a pointer of type sockaddr_infor ipv4 - doing static cast
        }
        else{
                return &(((struct sockaddr_in6*)sa)->sin6_addr); // return the address of a pointer of type sockaddr_in for ipv6 - doing static cast
        }
}


int SetupReciever(char* hostname, char* port, int& sock_fd){
        sock_fd = -2;
        struct addrinfo hints, *servinfo, *p;
        int return_val;
        char s[INET6_ADDRSTRLEN];

        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;

        // get the socket info for the connection to send data to
        if((return_val = getaddrinfo(hostname, port, &hints, &servinfo)) != 0){
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(return_val));
                return 1;
        }

        //loop through all the results and connect to the first socket we can 
        for(p = servinfo; p != NULL; p = p->ai_next){
                //connect to the socket
                if((sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
                        perror("client: socket");
                        continue;
                }

                // send a connection request
                if(connect(sock_fd, p->ai_addr, p->ai_addrlen) == -1){
                        close(sock_fd);
                        close(sock_fd);
                        continue;
                }
                break;
        }
        if (p == NULL){
                return 2;
        }

        //convert the recieved socket address to a charater array containing an ip address
        inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr), s, sizeof s);
        printf("clinet: connecting to %s\n", s);

        freeaddrinfo(servinfo);
	//return sock_fd;
	return 0;
}

void RecieveData(int sock_fd, queue <char*> &incoming_queue){
	while(1){
	        char buf[MAXDATASIZE];
		int numbytes;
		// if we recieve more bytes than the max allowable size, return error
	        if ((numbytes = recv(sock_fd, buf, MAXDATASIZE-1, 0)) == -1){
	               perror("recv");
	                exit(1);
	        }
	
	        buf[numbytes] = '\0'; //create the recieved data into a string by adding null terminator
		if(numbytes !=0){
       		 	//printf("client: recieved '%s'\n", buf);
		}
		incoming_queue.push(buf);
	}
        close(sock_fd);

}

int SetupSender(char* port, int& new_fd){
        int sock_fd;
	new_fd = -2; // listen on the socket file descriptor (sock_fd) and then have a new connection on the new_fd
        struct addrinfo hints, *server_info, *p;
        struct sockaddr_storage their_addr; // connector's address information 
        int return_val;
        socklen_t sin_size;
        char s[INET_ADDRSTRLEN];
        int rv;
        int yes = 1; // for setsockopt()
        memset(&hints, 0, sizeof hints); // hints value is 0000...0
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE; // Do not care if socket is ipv4 or ipv6

        // get the socket info for the machine you want to create a socket on 
        if((return_val = getaddrinfo(NULL, port, &hints, &server_info)) !=0){
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        }

        //loop through all the results for socket address and bind to the first socket we can 
        for(p = server_info; p != NULL; p = p->ai_next){
               // get the socket file descriptor
                if((sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
                        perror("server: socket"); // specify we have error in server socket
                        continue;
                }
                // try to resuse the port if it's still open in the kernel and bind it to the socket
                if(setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
                        perror("setsockpt");
                        exit(1);
                }
                // bind the port to the socket 
                if(bind(sock_fd, p->ai_addr, p->ai_addrlen) == -1){
                        close(sock_fd);
                        perror("server: bind");
                        continue;
                }
                break;
        }
        freeaddrinfo(server_info);

        // if we recieved no socket, we have nothing to bind to
        if(p == NULL){
                fprintf(stderr, "server: failed to bind\n");
        }
        if(listen(sock_fd, BACKLOG) == -1){
               perror("listen");
               exit(1);
        }

        printf("server: waiting for connections...\n");
        //loop to accept any connection requests
        while(new_fd == -2){
                sin_size = sizeof their_addr; //number of bytes for struct to hold client's socket address info
                new_fd = accept(sock_fd, (struct sockaddr *)&their_addr, &sin_size);
                if(new_fd == -1){
                       perror("accept");
                       continue;
                }
                inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);//convert their socket info into a character array containing their ip address
                printf("server: got connection from %s\n", s);


        }
        return new_fd;

}

void SendData(int new_fd, queue <char*> &outgoing_queue){
	while(1){
		if(!outgoing_queue.empty()){
			if(send(new_fd, outgoing_queue.front(), strnlen(outgoing_queue.front(), MAXDATASIZE), 0) == -1){
        			perror("send");
        		        close(new_fd);
           		     exit(0);
       			}
			outgoing_queue.pop();
		}
	}
}

void ConnectReciever(char* hostname, char* port, int& sock_fd){
	int return_code = -1; 
	
	while(return_code != 0){
		sock_fd = -2;
	 	return_code = SetupReciever(hostname, port, sock_fd);

	}
		

}


int main(int argc, char* argv[]){

	queue <char*> incoming_queue; 
	queue <char*> outgoing_queue; 
	int rc;	

	if(argc != 4){
		fprintf(stderr, "usage: ChatBot [my port number] [remote machine name] [remote port number] (ie. chatbot 6001 localhost 6000)\n");
                exit(1);
	} 

	

	int sock_fd = -2;
	int new_fd =-2;

		thread setup_sender_t(SetupSender, argv[1], ref(new_fd));
		thread setup_reciever_t(ConnectReciever, argv[2], argv[3], ref(sock_fd));

	
		setup_sender_t.join();
		setup_reciever_t.join();

		thread read_t(ReadKeyboard, ref(outgoing_queue));
		thread send_t(SendData, new_fd, ref(outgoing_queue));
		thread print_t(PrintMessage, ref(incoming_queue));
		thread recieve_t(RecieveData, sock_fd, ref(incoming_queue));
		read_t.join();
		
		cout << "READ THREAD ENDED" << endl;
		send_t.join();
		print_t.join(); 
		recieve_t.join();
	return 0; 
	

}
